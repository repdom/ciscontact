'use strict';

module.exports = function(app) {
  let mysql = app.dataSources.ciscontact;

  let modelos = [
    'AccessToken',
    'ACL',
    'Role',
    'RoleMapping',
    'miembroComite',
  ];

  mysql.autoupdate(modelos, function(err) {
    if (err) {
      throw err;
    }
    console.log(`===== ACTULIZANDO: modelo ${modelos} =====`);
    defaultRoles(app);
    crearUsuarioAdministrador(app);
    // crearConfiguracion(app);
  });
};

function defaultRoles(app) {
  let role = app.models.Role;

  let listOfRoles = [
        ['Administrador', 'Tiene acceso a todo el sistema.'],
        ['Contactador', 'Puede hacer las llamadas.'],
  ];

  listOfRoles.forEach(function(element) {
   // eslint-disable-next-line max-len
    role.findOrCreate({where: {name: element[0], description: element[1]}}, {name: element[0], description: element[1]},
            function(err, rol) {
              console.log(`===== OK ${rol.name} =====`);
            }
        );
  });
};

function crearUsuarioAdministrador(app) {
  let miembroComite = app.models.miembroComite;
  let rol = app.models.Role;
  let roleMapping = app.models.RoleMapping;

  // eslint-disable-next-line max-len
  miembroComite.findOrCreate({where: {username: 'manager', email: 'manager@manager.com'}}, {
    username: 'manager',
    email: 'manager@manager.com',
    password: 'manager123',
    nombre1: 'manager',
    nombre2: 'manager',
    apellido1: 'manager',
    apellido2: 'managel',
  },
        function(err, user) {
          if (err) return console.log(err);
          // eslint-disable-next-line max-len
          rol.findOrCreate({where: {name: 'Administrador', description: 'Tiene acceso a todo el sistema.'}}, {name: 'Administrador', description: 'Tiene acceso a todo el sistema.'},
                function(err, role) {
                    // if (err) return debug(err);
                  console.log(`===== OK ${role.name} =====`);

                  // eslint-disable-next-line max-len
                  roleMapping.findOrCreate({where: {roleId: role.id, principalId: user.id}}, {roleId: role.id, principalId: user.id, principalType: roleMapping.USER},
                        function(err, rolMapping) {
                          console.log(rolMapping);
                          if (err) return console.log(err);
                          console.log(`===== Rol asignado a ${user.username}`);
                        }
                    );
                }
            );
        }
    );
};

