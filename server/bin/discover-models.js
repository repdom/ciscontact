/**
 * Descubre y crea los modelos del proyecto en base a las tablas de la base de dato
 * que esten definidas a ser creadas en este documento.
 */

'use strict';

const loopback = require('loopback');
const promisify = require('util').promisify;
const fs = require('fs');
const writeFile = promisify(fs.writeFile);
const readFile = promisify(fs.readFile);
const mkdirp = promisify(require('mkdirp'));
const DATASOURCE_NAME = 'ciscontact'; // Este es el nombre del datasource
const dataSourceConfig = require('../datasources.json');
const ciscontact = new loopback.DataSource(dataSourceConfig[DATASOURCE_NAME]);
discover().then(
    success => process.exit(),
    error => { console.error('UNHANDLED ERROR:\n', error); process.exit(1); },
);
async function discover() {
    // Esta es la opcion que le permite a dataSource.discoverSchema() buscar las rela    ciones que hay entre las tablas.
    const options = { relations: true };

    // descubre los modelos con su relaciones
    const contactoSchemas = await ciscontact.discoverSchemas('contacto', options);
    const empresaSchemas = await ciscontact.discoverSchemas('empresa', options);
    const historiaSchemas = await ciscontact.discoverSchemas('historial', options);

    await mkdirp('common/models');
    const database = "ciscontact";
    await writeFile(
        'common/models/contacto.json',
        JSON.stringify(contactoSchemas[`${database}.contacto`], null, 2)
    );
    await writeFile(
        'common/models/empresa.json',
        JSON.stringify(empresaSchemas[`${database}.empresa`], null, 2)
    );
    await writeFile(
        'common/models/historial.json',
        JSON.stringify(historiaSchemas[`${database}.historial`], null, 2)
    );
    // Exponer los modelos via REST API
    const configJson = await readFile('server/model-config.json', 'utf-8');
    console.log('MODEL CONFIG', configJson);
    const config = JSON.parse(configJson);

    // El nombre que se le ponga a la variable config.Name será el nombre que se usará para la API.
    config.Contacto = { dataSource: DATASOURCE_NAME, public: true };
    config.Empresa = { dataSource: DATASOURCE_NAME, public: true };
    config.Historia = { dataSource: DATASOURCE_NAME, public: true };

    await writeFile(
        'server/model-config.json',
        JSON.stringify(config, null, 2)
    );    
}